package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if ((x == null) || (y == null))
            throw new IllegalArgumentException();
        if (y.size() < x.size())
            return false;
        int a = 0; // x index
        int b = 0; // y index
        int counter = 0;
        while (a < x.size()) {
            boolean c = true;
            while (c && (b < y.size())) {
                if (x.get(a).equals(y.get(b))) {
                    counter++;
                    c = false;
                }
                b++;
            }
            a++;
        }
        return counter == x.size(); //for pushing
    }
}
