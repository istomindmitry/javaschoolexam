package com.tsystems.javaschool.tasks.calculator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null)
            return null;
        String regEx = "([(]{1})([0-9+-\\\\*/.]+)([)]{1})";
        while (true) {
            Pattern pt = Pattern.compile(regEx);
            Matcher mt = pt.matcher(statement);

            if (mt.find()) {
                String expression = mt.group();
                statement = statement.replaceFirst(regEx, solve(expression));
                statement = statement.replaceFirst("\\(", "");
                statement = statement.replaceFirst("\\)", "");
            } else break;
        }
        if (statement.contains("(") || statement.contains((")"))) {
            return null;
        }
        System.out.println(statement);
        try {
            double result = Double.parseDouble(solve(statement));
            if (result == (int) result) {
                return String.valueOf((int) result);
            } else {
                return String.valueOf(result);
            }
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static String solve(String statement) {
        String regEx = "(([0-9]+([.]{0}|[.]{1}[0-9]+))([\\*/]{1})([-]?)([0-9]+([.]{1}[0-9]+|[.]{0})))";
        while (true) {
            Pattern pt = Pattern.compile(regEx);
            Matcher mt = pt.matcher(statement);

            if (mt.find()) {
                String expression = mt.group();
                statement = statement.replaceFirst(regEx, solveBasic(expression));
            } else break;
        }
        regEx = "(([-]?)([0-9]+([.]{0}|[.]{1}[0-9]+))([+-]{1})([0-9]+([.]{1}[0-9]+|[.]{0})))";
        while (true) {
            Pattern pt = Pattern.compile(regEx);
            Matcher mt = pt.matcher(statement);
            if (mt.find()) {
                String expression = mt.group();
                statement = statement.replaceFirst(regEx, solveBasic(expression));
            } else break;
        }
        return statement;
    }

    private static String solveBasic(String statement) {
        double result = 0;
        String[] array;
        if (statement.contains("+")) {
            array = statement.split("\\+");
            result = Double.parseDouble(array[0]) + Double.parseDouble(array[1]);
        } else if (statement.contains("*")) {
            array = statement.split("\\*");
            result = Double.parseDouble(array[0]) * Double.parseDouble(array[1]);
        } else if (statement.contains("/")) {
            array = statement.split("/");
            result = Double.parseDouble(array[0]) / Double.parseDouble(array[1]);
        } else if (statement.contains("-")) {
            if (statement.startsWith("-")) {
                array = statement.split("-");
                result = -1 * Double.parseDouble(array[1]) - Double.parseDouble(array[2]);
            } else {
                array = statement.split("-");
                result = Double.parseDouble(array[0]) - Double.parseDouble(array[1]);
            }
        }
        return String.valueOf(result);
    }
}
