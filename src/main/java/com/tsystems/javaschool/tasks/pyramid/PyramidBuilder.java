package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int figure_power = 0;
        int pyramid_height;
        if (inputNumbers == null)
            throw new CannotBuildPyramidException();
        if (inputNumbers.size() > 1000)
            throw new CannotBuildPyramidException();
        for (int i = 0; i < inputNumbers.size(); i++)
            if (inputNumbers.get(i) == null)
                throw new CannotBuildPyramidException();
        for (pyramid_height = 0; figure_power < inputNumbers.size(); pyramid_height++) {
            figure_power += 1 + pyramid_height;
        }
        if (figure_power > inputNumbers.size())
            throw new CannotBuildPyramidException();
        inputNumbers.sort((a, b) -> (a.intValue() < b.intValue()) ? -1 : 1);
        int pyramid_width = 2 * pyramid_height - 1;
        int[][] pyramid = new int[pyramid_height][pyramid_width];
        for (int i = 0; i < pyramid_height; i++)
            for (int j = 0; j < pyramid_width; j++)
                pyramid[i][j] = 0;

        int start_position = (pyramid_width - 1) / 2;
        int count = 0;
        for (int i = 0; i < pyramid_height; i++) {
            for (int j = 0; j < i + 1; j++) {
                pyramid[i][start_position + j * 2] = inputNumbers.get(count);
                count++;
            }
            start_position--;
        }
        return pyramid;
    }


}
